function doTXTcall(type, url, callback) {
	var xmlhttp = new XMLHttpRequest()
	xmlhttp.onreadystatechange = function () {
		if (xmlhttp.readyState == XMLHttpRequest.DONE && xmlhttp.status == 200) {
			var data = xmlhttp.responseText
			if (callback) callback(data)
		}
	}
	xmlhttp.open(type, url, true)
	xmlhttp.send()
}

function parsePages(data) {
	var pages = data.split(/{{PAGE}}/)
	pages.forEach(function(item, i){
		var content = Markdown(item);
		var page = document.createElement('div')
		page.id = 'page'+i
		page.className = 'page'
		page.setAttribute('data-position', i)
		var folio = '<div class="folio">'+ i + "/" + (pages.length - 1) + '</div>'
		page.innerHTML = content
		page.innerHTML += folio

		document.getElementById("screen").innerHTML += page.outerHTML
	})
	slide('page')
}



function slide(classPage){
	var pages = document.getElementsByClassName(classPage)
	var lenPages = pages.length - 1
	
	if (window.location.hash) {
		var id = window.location.hash.replace('#', '')
		if (document.getElementById(id)) {
			document.getElementById(id).classList.add('active') 
		}else
		{
			pages[0].classList.add('active')
		}
	} else {
		pages[0].classList.add('active')

	}

	document.addEventListener('click', function(event) {
		var activePage = document.getElementsByClassName('active')[0] 
		activePage.classList.remove('active')
		if (activePage.getAttribute('data-position') !== lenPages.toString()){
			activePage.nextSibling.classList.add('active')
			window.location.hash = activePage.nextSibling.id
		} else {
			pages[0].classList.add('active')
			window.location.hash = pages[0].id
		}
	})

	document.addEventListener('keydown', (event) => {
		var name = event.key;
		var code = event.code;
		if (code == "ArrowRight") {
			var activePage = document.getElementsByClassName('active')[0] 
			activePage.classList.remove('active')
			if (activePage.getAttribute('data-position') !== lenPages.toString()){
				activePage.nextSibling.classList.add('active')
				window.location.hash = activePage.nextSibling.id
			} else {
				pages[0].classList.add('active')
				window.location.hash = pages[0].id
			}
		} else if (code == "ArrowLeft") {
			var activePage = document.getElementsByClassName('active')[0] 
			activePage.classList.remove('active')
			if (activePage.getAttribute('data-position') !== '0'  ){
				activePage.previousSibling.classList.add('active')
				window.location.hash = activePage.previousSibling.id
			} else {
				pages[lenPages].classList.add('active')
				window.location.hash = pages[lenPages].id
			}
		}
	}, false)

	console.log(pages)
}


window.addEventListener('DOMContentLoaded', () => {

	doTXTcall(
		"GET",
		"content.md",
		function (data) {
			parsePages(data)

		}
	)

})
