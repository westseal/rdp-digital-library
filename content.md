# Petite présentation du Rideau de perles

### Dans le cadre du workshop *Digital Library*

## Utilise les flêches pour naviguer < >

{{PAGE}}

# Le rideau de perles ?

{{PAGE}}

![rdpmodelisation.jpg](img/rdpmodelisation.jpg)

{{PAGE}}

## Comment est né le rideau de perles ?

{{PAGE}}

Créé en 2018, le rideau de perles prend pour point de départ le manque d'une bibliothèque au sein de l'école reflétant sa politisation et de la récupération de l'infokiosque existant.

{{PAGE}}

Le rideau de perles se compose d'étudiant·es actuel·les et ancien·nes de l'erg et de La Cambre mais est ouvert à touxtes. C'est un projet bénévole étudiant qui fait parti des nombreux projets transversaux de l'erg.

{{PAGE}}

## L'erg ?

{{PAGE}}

![Capture d’écran du 2023-03-19 13-41-17.png](img/Capture%20d%E2%80%99%C3%A9cran%20du%202023-03-19%2013-41-17.png)Site de l'école de recherche graphique basé sur un système d'écriture et d'édition de contenu collaboratif (le wiki).

{{PAGE}}

L'erg est une école à Bruxelles qui existe depuis les années 70 et dépend de l'école d'art de Saint-Luc. mettant en avant une pédagogie alternative des autres écoles d'art qui reposent sur un fonctionnement davantage académique / traditionnel. L'erg met l'accent sur la transdisciplinarité à travers l'alliance de cours théoriques et pratiques. Les traditionnels "départements" sont remplacés par des orientations qui visent à décloisonner les pratiques. Le rideau de perles fait partie de l'espace transversal comme d'autres initiatives.

{{PAGE}}

![img/diagrame erg (2).png](img/200.png)

Diagramme de l'erg représentant l'organisation des cours à travers les 5 années, bac jusqu'au master. Les espaces transversaux se situent de manière informelle en addition à cette structure.


{{PAGE}}
[https://wiki.erg.be/m/#À\_propos_du_diagramme](https://wiki.erg.be/m/#%C3%80_propos_du_diagramme)
<https://metalouis.hotglue.me/>

{{PAGE}}

D'autres espaces transversaux :

* TPG@erg
* SaferSpace
* Ergote radio
* La BàG (boîte à gants) : récupérathèque de l'école
* EàT (erg à table) : cantine étudiante
* La bobine : labo de développement de film
* Le Torchon : journal de l'école
* Perm édit / mardi soir éditions

{{PAGE}}

# Comment avez-vous accès aux documents (livres, articles...) dont vous avez besoin ? (bibliothèque de l'école ? bibliothèques *pirates* ? partage de fichier pair à pair ? salon de discussion sur discord ?...)

{{PAGE}}

# Bibliothèque pirate : copie, piraterie, archive

{{PAGE}}

> "Nous sommes toustes les gardien·nes du savoir, les gardien·nes des mêmes infrastructures dont nous dépendons pour produire de la connaissance, les gardien·nes d'un matri·patrimoine fertile mais fragile. Être un.e gardien·ne c'est, de facto, télécharger, partager, lire, écrire, commenter, éditer, numériser, archiver, entretenir ces bibliothèques, les rendre accessibles. Il s'agit d'être utile aux biens communs de la connaissance, non d'en devenir propriétaire."


Texte collectif, En solidarité avec Library Genesis et Sci-Hub, Traduit par Bertrand Marilier, revu par Luuse et Alexia de Visscher, mars 2022 [2015] <http://archivism.luuse.io/custodians/index.html>

{{PAGE}}

## Fabrication du bookscanner

{{PAGE}}

![IMG_20190911_154046.jpg](img/IMG_20190911_154046.jpg)La bête

{{PAGE}}

![IMG_20190911_132446.jpg](img/IMG_20190911_132446.jpg){{PAGE}}

![IMG_20190320_172223.jpg](img/IMG_20190320_172223.jpg){{PAGE}}

Plans du bookscanner (coupe transversale)

![scanner-design.png](img/819-1024-max.jpg)

{{PAGE}}

Plus d'info : 

<https://www.diybookscanner.org>

[__https://makezine.com/projects/make-41-tinkering-toys/diy-book-scanner/__](https://makezine.com/projects/make-41-tinkering-toys/diy-book-scanner/)

<https://pads.erg.be/p/bookscanner>

<https://luuse.gitlab.io/recherche_formes-archives/book-scanner/site-documentation/>

{{PAGE}}

![schema.png](img/schema.png){{PAGE}}

Pi-scan est le programme utilisé pour contrôler les caméras 
<https://github.com/Tenrec-Builders/pi-scan>

{{PAGE}}

## Quelques termes...

{{PAGE}}

Au fur et à mesure que le livre opère un tournant vers le numérique, les bibliothèques publiques ne semblent pas emprunter les mêmes voies issues de cette transformation majeure. Avec de moins en moins de barrières techniques, plus que jamais, n’importe qui peut avoir accès de n’importe où aux livres, là où la réalité juridique semble bien plus compliquée en matière de partage et de prêt des livres électroniques (à l’instar des films ou des pdf…).

{{PAGE}}

> "L’information, c’est le pouvoir. Mais comme pour tout pouvoir, il y a ceux qui veulent le garder pour eux. Le patrimoine culturel et scientifique mondial, publié depuis plusieurs siècles dans les livres et les revues, est de plus en plus souvent numérisé puis verrouillé par une poignée d’entreprises privées."

Aaron Schwarz, Guerilla Open Access Manifesto, 2008

{{PAGE}}

### enclosures 

La lettre intitulée En solidarité avec Library Genesis et Sci-Hub, dénonçant la main mise des entreprises sur les publications de recherche scientifique rappelle qu’Internet, malgré ses dimensions politique et technique facilitant le libre partage en réseau, demeure vulnérable face aux enclosures. Ce terme désigne les mouvements d’appropriation d’une ressource auparavant gérée collectivement par une poignée d’individus. Il tire son origine du mouvement de privatisation des champs et la mise en place de droits de propriété sur des pâturages cultivés en commun, dès le début du XIIe siècle en Angleterre et au pays de Galles. Internet voit émerger un second mouvement d’enclosures lié aux conditions d’accès juridiques (licences), géographiques et techniques (interopérabilité) qui limitent considérablement l’accès à la connaissance.

{{PAGE}}

quelques bibliothèques pirates :

* [shadowlibraries.github.io](http://shadowlibraries.github.io)
* Public Library
* Monoskop
* [aaarg.fail](http://aaarg.fail)
* UBU-web
* sci-hub
* libgen
* Z-library (RIP😭)
* [Trans ](https://transreads.org/)[Reads.org](http://Reads.org)
* [zinzinzine.net](http://zinzinzine.net)
* [infokiosques.net](http://infokiosques.net)


{{PAGE}}

### livres en commun 

Les documents numérisés sont des ressources partagées, gérées par une communauté d’usagers qui institue des règles de fonctionnement en vue d’assurer la durabilité du dispositif. Ils constituent des biens communs informationnels, puisque leur numérisation permet la démultiplication du savoir, au-delà des propriété rivale et exclusive des livres physiques.

{{PAGE}}

### piraterie

L’histoire de ces bibliothèques parallèles est intimement liée à une forme de culture « pirate ». Par le bricolage, la transformation et le détournement des outils numériques, ces pratiques sont à l'origine un moyen d'expérimenter, de partager et de s'approprier des technologies au sein d'une communauté d'utilisateurs.

> « L’expérience pirate passe alors par la création de communautés spécifiques, souvent dans les interstices ou les marges des sociétés, dans un aller retour permanent entre les espaces normaux dont on transgresse les frontières et les lois et les espaces pirates où se déploient d’autres logiques ».

Hayat, S., Paloque-Bergès, C. (2014). Transgressions pirates, Revue Tracés, n°26 « Pirater ».

{{PAGE}}

> « Mais toutes ces actions se déroulent dans l’ombre, de façon souterraine. On les qualifie de «  vol  » ou bien de «  piratage  », comme si partager une abondance de connaissances était moralement équivalent à l’abordage d’un vaisseau et au meurtre de son équipage. Mais le partage n’est pas immoral, c’est un impératif moral ».

Aaron Schwarz, Guerilla Open Access Manifesto, 2008

{{PAGE}}

### copie 

La copie privée fait l’objet d’un flou juridique. Elle est autorisée dans un usage privé restreint au partage au sein du cercle familial. Or la loi n’indiquant pas la provenance des objets à copier, certaines bibliothèques organisent des copy-party. Nous souhaitons permettre aux acteurices de la bibliodiversité de copier les documents numérisés sur leur propre matériel, tout en leur laissant le libre choix de les rediffuser à autrui.

La numérisation et la copie ne sont pas uniquement des procédés de reproduction comme il est laissé à entendre, mais bien des pratiques de production de formes.

{{PAGE}}

### dispositifs de propriété intellectuelle 

Il existe aujourd’hui des manières multiples d'assurer la protection du droit d’auteurice et de la propriété intellectuelle sur une production culturelle et intellectuelle (licence, DRM). Nous ne sommes pas satisfait·es des systèmes qui existent actuellement car ils renforcent les inégalités de revenus et la précarisation des auteurices sans garantir un accès correct à la connaissance.

Vidéo explicative sur les DRM (3min40 transcript en barre d'info) :

<https://www.youtube.com/watch?v=kbv4rvxAO1o&pp=ygUWRFJNIHF1J2VzIGNlIHF1ZSBjJ2VzdA%3D%3D>

{{PAGE}}

## Pandora

{{PAGE}}

![1bis.png](img/1bis.png)[pandora.erg.be](http://pandora.erg.be) (dispo en réseau local, uniquement via le wifi de l'erg (mdp: erg_87)

{{PAGE}}

![3.png](img/3.png){{PAGE}}

![5.png](img/5.png){{PAGE}}

# Politisation de l'école, choix des ouvrages

{{PAGE}}

> "What might happen if we reclaimed the art school library as a space for different kinds of reading bodies ?  What might this mean ? Could we imagine architectural paradigms for reading in which the library space was a forum for a living community, who cared for books and read together ?"

Heide Hinrichs, Jo-ey Tang et Elizabeth Haines, shelf documents : art library as a practice, 2021.

{{PAGE}}

Nous réfléchissons à l'écriture d'un texte servant de guide conduisant les choix de sélection des ouvrages au sein de la bibliothèque.

**Titre :**

**Auteur·ice:**

(si plusieurs livres, marquez le reste aux verso)

**Date de don :**

**Nom donateur·ice**: (optionnelle)

**Pourquoi tu veux qu'il soit aux Rideau de Perles ?** (optionnelle)

**Anecdote Fun liée au livre :** (optionnelle)

**Si le livre est refusé ; Veut-tu le récupérer ?**

O Oui, rendez le-moi. O Non, faite en ce que vous pensez le mieux.

**Si non veut-tu quand même être informé.e ?**

O Oui, informer-moi. O Non.

**Si oui à une ou 2 des question aux dessus ; laisses un mail de contacte :**

{{PAGE}}

# Par les étudiant·es pour les étudiant·es : partage de savoirs, horizontalité, passation

# Vie de la bibliothèque

{{PAGE}}

![flyer-scanparty (3).gif](img/flyer-scanparty.gif)

Scan party

{{PAGE}}

![scan-party_26-10-18_affiche (2).jpg](img/scan-party_26-10-18_affiche.jpg)

Scan party (2018)

{{PAGE}}

Festival du cinéma sauvage (2019)

{{PAGE}}

![vignette radio finale.jpg](img/vignette%20radio%20finale.jpg)

Émission de radio avec Ergote (2021)

[https://www.mixcloud.com/Ergote_Radio/le-rideau-de-perles-\_-présentation-du-collectif-lectures-et-infos/](https://www.mixcloud.com/Ergote_Radio/le-rideau-de-perles-_-pr%C3%A9sentation-du-collectif-lectures-et-infos/)

Tanscripte : <https://docs.google.com/document/d/1LFiFgX8Kt9b7Ol9L9kqekR8GS75Rdbz5XtdkFanqh4w/edit#heading=h.y8li8oio7c6p>

{{PAGE}}

Workshop copie de livre + reliure (2022)


{{PAGE}}

![Capture d’écran du 2023-03-19 14-00-30.png](img/wsencre.png)

Workshop hacking de l'encre d'imprimante (2022)
{{PAGE}}

![photo WS refil cartouche encre (2).jpg](img/photo%20WS%20refil%20cartouche%20encre%20%282%29.jpg)
{{PAGE}}
![photo WS refil cartouche encre (1).jpg](img/photo%20WS%20refil%20cartouche%20encre%20%281%29.jpg)

{{PAGE}}

![affiche club de lecture rdp.jpg](img/affiche%20club%20de%20lecture%20rdp.jpg)

Club de lecture (2021-2022)

{{PAGE}}

![affiche grosse perm scan zen.jpg](img/affiche%20grosse%20perm%20scan%20zen.jpg)

Formation au scanner et au traitement des images (2019 – maintenant)

{{PAGE}}

![tri fddl au CE (2).jpg](img/tri%20fddl%20au%20CE%20%282%29.jpg) {{PAGE}}

![tri fddl au CE (1).jpg](img/tri%20fddl%20au%20CE%20%281%29.jpg)

Récup et tri des livres du Conseil Étudiant (2022)

{{PAGE}}

## Permanences

{{PAGE}}

![affiche permanence 2021.jpg](img/affiche%20permanence%202021.jpg)

{{PAGE}}

![Capture.PNG](img/Capture.PNG)

{{PAGE}}

![photo perlm litté trans sept 2022 (2) (2).jpg](img/permtrans1.jpg){{PAGE}}

![photo perlm litté trans sept 2022 (1).jpg](img/permtrans3.PNG){{PAGE}}


{{PAGE}}

![Capture d’écran du 2023-03-19 14-01-09.png](img/Capture%20d%E2%80%99%C3%A9cran%20du%202023-03-19%2014-01-09.png)Permanences 2022

{{PAGE}}

![Capture d’écran du 2023-03-19 13-49-38.png](img/Capture%20d%E2%80%99%C3%A9cran%20du%202023-03-19%2013-49-38.png)

{{PAGE}}

![affiche.jpg](img/affiche.jpg)

{{PAGE}}

## Ce qu'on fait au quotidien

{{PAGE}}

* Permanence 1x/mois (parfois à thème)
* Formations scanner (scan + traitement scan)
* Chercher et lire des nouveaux livres/textes
* Cataloguer les nouveaux ajouts sur zotero
* Checker les retours de livre et relancer les emprunts qui ont beaucoup de retard
* Répondre aux demandes et messages (mail, insta, mattermost)
* Faire de la com pour parler du projet et annoncer les événements
* Réunions d'organisation entre nous

{{PAGE}}

![Capture d’écran du 2023-03-19 14-00-15.png](img/Capture%20d%E2%80%99%C3%A9cran%20du%202023-03-19%2014-00-15.png)Régis le registre

{{PAGE}}

## Ce qu'on aimerai faire dans le futur

**Projet**

* Charte accessibilité
* Charte/texte qui situe contenu et limite de la bibli (critère de refus, note contenu/médiation,...)
* Facilité et clarifier les don de livre 
* Facilité accès des mémoires pour les étudiant.e
* \+ livre audio et epub
* Construire une boîte à dons 
* Avoir une chronique dans le journal de l'école
* Cartographie d'autres bibliothèques pirate / autogérées / militante / autonome, numérique ou physique.

{{PAGE}}
**Evènement**

* organiser des permanences sur des thèmes spécifiques (sabotage, critique école d'art et lutte étudiant.e/travailleur.se des arts, transport en commun,...)
* Organiser des lecture collective, radiophonique ou en arpentage. D'auteuri.ces externe à l'erg ou d'étudient.e.s.
* Parler bibli : Inviter des auteurices, éditeurices, bibliothécaires, archivistes... 
* Re-Workshop duplicata de livre 
* Workshop illustration/amélioration/création page wiki (dans l'idée du collectif les sans pagEs)
* Relancer le club de lecture

Liste non-exhaustive ;)

{{PAGE}}

## Zotero

{{PAGE}}

![zotero.png](img/zotero.png)Utilisation du logiciel de gestion bibliographique Zotero pour répertorier la collection de ressources physiques et numériques.


{{PAGE}}
Il permet également d'avoir accès depuis chez soi aux contenu de pandora qui elle n'est disponible que à l'erg. <https://www.zotero.org/groups/2622649/rideau_de_perles_-_rdp/library>
{{PAGE}}

![interfacezotero.png](img/interfacezotero.png)Création d'une interface d'affichage du catalogue en cours en récupérant les données contenues dans Zotero.

{{PAGE}}

### système de classification 

Tout système de classification des ressources est situé dans un contexte socio-politique et historique donné, qui influence notre façon d’ordonner les savoirs.

comment penser un classement alternatif ?

exemples :

* par pronom narratif (je, tu elle...)
* par hauteur
* par la présence ou non d'un animal